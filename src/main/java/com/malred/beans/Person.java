package com.malred.beans;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author malguy-wang sir
 * @create ---
 */
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Component
public class Person {
    private int id;
    private String name;
    private int age;
    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }
}
