package com.malred;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.pool.DruidDataSourceFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.sql.Connection;
import java.util.Properties;

/**
 * @author malguy-wang sir
 * @create ---
 */
@Configuration
@ComponentScan("com.malred")
@EnableTransactionManagement
public class config {
    @Bean
    public static DataSource dataSource() throws Exception{
       DruidDataSource dataSource = new DruidDataSource();
       dataSource.setUsername("root");
       dataSource.setPassword("q2k0f0n3");
       dataSource.setUrl("jdbc:mysql://localhost:5408/test");
       dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        return dataSource;
    }
    @Bean
    public JdbcTemplate jdbcTemplate()throws Exception{
        return new JdbcTemplate(dataSource());
    }
    @Bean
    public PlatformTransactionManager transactionManager()throws Exception{
        return new DataSourceTransactionManager(dataSource());
    }
}
