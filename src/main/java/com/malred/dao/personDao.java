package com.malred.dao;

import com.malred.beans.Person;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author malguy-wang sir
 * @create ---
 */
@Repository
public interface personDao {
    public void insert(Person person);
    public void deleteById(int id);
    public Person selectById(int id);
    public List<Person> showAll();
    public void update(Person person);
}
