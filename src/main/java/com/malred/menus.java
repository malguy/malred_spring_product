package com.malred;

import com.malred.beans.Person;
import com.malred.service.personService;
import lombok.extern.slf4j.Slf4j;

import java.util.Scanner;

/**
 * @author malguy-wang sir
 * @create ---
 */
public class menus {
    private static personService myService;
    private static Scanner scanner = new Scanner(System.in);
    public static void menu(personService service){
        myService = service;
        boolean flag = true;
        while(flag){
            System.out.println("菜单：\n" +
                    "1，添加\n" +
                    "2，修改\n" +
                    "3，删除\n" +
                    "4，查找\n" +
                    "5，退出");
            int key = scanner.nextInt();
            switch (key){
                case 1:
                    choice(key,scanner);
                    break;
                case 2:
                    choice(key,scanner);
                    break;
                case 3:
                    choice(key,scanner);
                    break;
                case 4:
                    choice(key,scanner);
                    break;
                case 5:
                    flag=false;
                    break;
            }
            System.out.println("当前的列表");
            myService.showAll();
        }
    }
    public static void choice(int key,Scanner scanner){
        switch (key){
            case 1:
                System.out.println("输入要添加的人的名字");
                String name = scanner.next();
                System.out.println("输入要添加的人的年龄");
                int age = scanner.nextInt();
                myService.insert(new Person(name,age));
                break;
            case 2:
                System.out.println("输入要修改的人的id");
                int num = scanner.nextInt();
                System.out.println("输入要修改成的名字");
                String name1 = scanner.next();
                System.out.println("输入要修改成的年龄");
                int age1 = scanner.nextInt();
                myService.update(new Person(num,name1,age1));
                break;
            case 3:
                System.out.println("输入要删除的id");
                int n = scanner.nextInt();
                myService.deleteById(n);
                break;
            case 4:
                System.out.println("输入要查找的id");
                int id = scanner.nextInt();
                myService.selectById(id);
                break;
        }
    }
}
