package com.malred.service;

import com.malred.beans.Person;
import com.malred.dao.personDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author malguy-wang sir
 * @create ---
 */
@Service
public class personService {
    @Autowired
    private com.malred.dao.personDao personDao;
    public Person selectById(int id){
        System.out.println(personDao.selectById(id));
        return personDao.selectById(id);
    }
    public void showAll(){
        for (Person person : personDao.showAll()) {
            System.out.println(person);
        }
    }
    public void deleteById(int id){
        personDao.deleteById(id);
    }
    public void insert(Person person){
        personDao.insert(person);
    }
    public void update(Person person){
        personDao.update(person);
    }
}
